package rmi;

import model.MedicationPlan;
import model.Patient;

import java.rmi.Remote;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;

public interface DemoRemote extends Remote{
    public List<MedicationPlan> getMedicationPlan() throws Exception;
    public List<Patient> getPatients() throws Exception;
}
