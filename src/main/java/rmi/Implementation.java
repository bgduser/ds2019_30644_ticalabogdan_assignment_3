package rmi;

import model.MedicationPlan;
import model.Patient;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class Implementation implements DemoRemote {

    private final static String selectAllStatementString = "SELECT * FROM patient;";


    @Override
    public List<MedicationPlan> getMedicationPlan() throws Exception {

        List<MedicationPlan> list = new ArrayList<MedicationPlan>();
        try {
            Connection con = ConnectionFactory.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM medication_plan;");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                int id = rs.getInt("id");
                String dailyInterval = rs.getString("daily_interval");
                String periodTreatment = rs.getString("period_treatment");

                // Setare valori

                MedicationPlan medicationPlan = new MedicationPlan();
                medicationPlan.setId(id);
                medicationPlan.setDailyInterval(dailyInterval);
                medicationPlan.setPeriodOfTreatment(periodTreatment);

                list.add(medicationPlan);
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        return list;

    }

    @Override
    public List<Patient> getPatients() throws Exception {
        return null;
    }

}
/*
    @Override
    public List<Patient> getPatients() throws Exception {

        Connection con = ConnectionFactory.getConnection();
        ResultSet rs = null;
        PreparedStatement selectAllStatement = null;
        List<Patient> list = new ArrayList<Patient>();
        try {
            selectAllStatement = con.prepareStatement(selectAllStatementString);
            rs = selectAllStatement.executeQuery();
            //Extract data from result set
            while (rs.next()) {
                // Retrieve by column name
                int id = rs.getInt("id_patient");
                String name = rs.getString("name");
                String gender = rs.getString("gender");
                String email = rs.getString("email");
                String address = rs.getString("address");
                String medR = rs.getString("medical_record");
                ///Date birth_date = rs.getDate("birth_date");

                // Setting the values
                Patient patient = new Patient();

                patient.setId(id);
                patient.setName(name);
                patient.setEmail(email);
                patient.setGender(gender);
                patient.setAddress(address);
                patient.setMedicalRecord(medR);

                list.add(patient);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(selectAllStatement);
            ConnectionFactory.close(con);
        }
        return list;
    }
}*/

