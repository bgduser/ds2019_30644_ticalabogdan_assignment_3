package server;

import model.MedicationPlan;
import rmi.DemoRemote;
import rmi.Implementation;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

public class Server extends Implementation {
    public Server() { }

    public static void main(String args[]) throws RemoteException {

        Implementation obj = new Implementation();
        try {

            //Registry registry = LocateRegistry.getRegistry();
           // Registry registry = LocateRegistry.createRegistry(1099);
            DemoRemote stub = (DemoRemote) UnicastRemoteObject.exportObject(obj, 0);
            Registry registry ;
            try {
                registry  = LocateRegistry.createRegistry(1099);
                System.out.println("java RMI registry created.");

            } catch(Exception e) {
                System.out.println("Using existing registry");
                registry  = LocateRegistry.getRegistry();
            }
            registry.rebind("DemoRemote", stub);
            System.err.println("Server ready");

//           List<MedicationPlan> list = (List)stub.getMedicationPlan();
//
//            for(MedicationPlan s : list) {
//                System.out.println("ID: " + s.getId());
//                System.out.println("dailyInterval: " + s.getDailyInterval());
//                System.out.println("periodOfTreatment: " + s.getPeriodOfTreatment());
//            }

        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
