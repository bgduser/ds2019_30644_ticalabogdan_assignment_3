package model;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;



public class MedicationPlan implements Serializable {

    private Integer id;
    private String dailyInterval;
    private String periodOfTreatment;

    private Set<Medication> medications;

    private Patient patient;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public MedicationPlan() {

    }

    public MedicationPlan(String dailyInterval, String periodOfTreatment, Set<Medication> medications, Patient patient) {
        this.dailyInterval = dailyInterval;
        this.periodOfTreatment = periodOfTreatment;
        this.medications = medications;
        this.patient = patient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDailyInterval() {
        return dailyInterval;
    }

    public void setDailyInterval(String dailyInterval) {
        this.dailyInterval = dailyInterval;
    }

    public String getPeriodOfTreatment() {
        return periodOfTreatment;
    }

    public void setPeriodOfTreatment(String periodOfTreatment) {
        this.periodOfTreatment = periodOfTreatment;
    }

    public Set<Medication> getMedications() {
        return medications;
    }

    public void setMedications(Set<Medication> medications) {
        this.medications = medications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlan that = (MedicationPlan) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(dailyInterval, that.dailyInterval) &&
                Objects.equals(periodOfTreatment, that.periodOfTreatment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dailyInterval, periodOfTreatment);
    }
}
