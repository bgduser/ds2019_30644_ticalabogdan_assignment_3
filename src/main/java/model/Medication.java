package model;

import java.util.Objects;
import java.util.Set;

public class Medication {
    private Integer id;
    private String name;
    private String effects;
    private String dosage;

    private Set<MedicationPlan> medicationPlans;

    public Medication() {
    }

    public Medication(int id, String name, String effects, String dosage) {
        this.id = id;
        this.name = name;
        this.effects = effects;
        this.dosage = dosage;
    }

    public Medication(String name, String effects, String dosage) {
        this.name = name;
        this.effects = effects;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEffects() {
        return effects;
    }

    public void setEffects(String effects) {
        this.effects = effects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Medication that = (Medication) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(effects, that.effects) &&
                Objects.equals(dosage, that.dosage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, effects, dosage);
    }
}
