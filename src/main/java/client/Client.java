package client;

import model.MedicationPlan;
import rmi.DemoRemote;
import view.App;

import java.awt.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

public class Client {

    private Client() {
    }

    public static void main(String[] args) throws Exception {

       /* EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    App window = new App();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });*/

        try {
            Registry registry = LocateRegistry.getRegistry(1099);

            // Looking up the registry for the remote object
            DemoRemote stub = (DemoRemote) registry.lookup("DemoRemote");

            // Calling the remote method using the obtained object
            List<MedicationPlan> lists = stub.getMedicationPlan();

            for(MedicationPlan s : lists) {
                System.out.println("ID: " + s.getId());
                System.out.println("dailyInterval: " + s.getDailyInterval());
                System.out.println("periodOfTreatment: " + s.getPeriodOfTreatment());
            }

        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();

        }
    }
}

